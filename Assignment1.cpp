#include <iostream>
using std::cout;
using std::cin;
using std::endl;
#include <string>
using std::string;
#include <math.h>
int genSSID();
int genGrades();
string genMajor();
string genFname();
string genLname();
void genRecord(int myAmount);

int main(){
  srand(time(NULL));                  //For future reference you only need to implement this once in the main
  int myAmount;

  cout << "How many student records do you want?" << endl;
  cin >> myAmount;

  genRecord(myAmount);


  return 0;
}

int genSSID(){

	int myAreacode = 0;
	int myGroupnum = 0;
	int mySerialnum = 0;
	int mySSID = 0;
    myAreacode = rand() % 1000;
    myGroupnum = rand() % 100;
    mySerialnum = rand() % 10000;
    mySSID = myAreacode*1000000 + myGroupnum*10000*mySerialnum;
    mySSID = abs(mySSID);
	if(mySSID > 100000000){
		mySSID = mySSID/10;
	}
    /*cout << "SSID: " << myAreacode << '-' << myGroupnum <<'-' << mySerialnum;                       //Ignore this, just for testing 
    */                                                                         
  return mySSID;
}

string genMajor(){

  string myMajor;

    int myRand = rand() % 100;
     if(myRand <50){
       myMajor = "CS";
     }
     else if(myRand > 50 && myRand < 67){
       myMajor = "EE";
     }
     else if(myRand > 67 && myRand < 83){
       myMajor = "CE";
     }
     else if(myRand > 84 && myRand < 100){
       myMajor = "ME";
    }

   /*cout << myMajor << endl;
   */

  return myMajor;
}

string genFname(){

  static const char lowerAlpha[27] = "abcdefghijklmnopqrstuvwxyz";
  static const char upperAlpha[27] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  char myRand1;
  char myRand2;
  string myFname;
  myFname.clear();

  myRand1 = upperAlpha[rand() % (26-0 + 0) + 0];

  const int nameLength = rand()%(12-1 + 1) + 1; //int randNum = rand()%(max-min + 1) + min;
  myFname = myRand1;

  for(int i = 0; i < nameLength; i++){

    myRand2 = lowerAlpha[rand() % (26-0 + 1) + 0];

    myFname = myFname + myRand2;

  }

    return myFname;
}
string genLname(){                            //same thing as genFname

  static const char lowerAlpha[27] = "abcdefghijklmnopqrstuvwxyz";
  static const char upperAlpha[27] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  char myRand1;
  char myRand2;
  string myLname;

  myRand1 = upperAlpha[rand() % (26-0 + 0) + 0];

  const int nameLength = rand()%(12-1 + 1) + 1; //int randNum = rand()%(max-min + 1) + min;
  myLname = myRand1;

  for(int i = 0; i < nameLength; i++){

    myRand2 = lowerAlpha[rand() % (26-0 + 1) + 0];

    myLname = myLname + myRand2;
  }




  return myLname;
}

float genGrade(int mean, int stddev){     //Box muller method to develop grades. Taken from
    static float n2 = 0.0;                //stackoverflow which was taken from a now dead link
    static int n2_cached = 0;             //because I couldn't understand the link given
    if (!n2_cached){
        float x, y, r;
        do{
            x = 2.0*rand()/RAND_MAX - 1;
            y = 2.0*rand()/RAND_MAX - 1;

            r = x*x + y*y;
        }
        while (r == 0.0 || r > 1.0);{
            float d = sqrt(-2.0*log(r)/r);
            float n1 = x*d;
            n2 = y*d;
            float result = n1*stddev + mean;
            n2_cached = 1;
            return result;
        }
    }
    else{
        n2_cached = 0;
        return n2*stddev + mean;
    }
}

float genMidterm1(){
  return genGrade(60,12);
}

float genMidterm2(){
  return genGrade(55,13);
}

float genmyFinal(){
  return genGrade(46,14);
}

void genRecord(int myAmount){

  for(int i = 0; i < myAmount; ++i){
  cout << "First Name: " << genFname() << ' ' << "Last Name: " << genLname() << " | " <<"Major: " << genMajor() << " | " <<"Midterm1: " << genMidterm1() << " | " <<"Midterm2: " << genMidterm2() << " | " <<"Final: " << genmyFinal() << " | " <<"SSID: " << genSSID() << " | " << endl << endl;
  }
}
